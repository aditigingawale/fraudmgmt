package com.fcm.krypton_bank.Entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.annotation.Nonnull;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity

@Table(name = "Case_Detail")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class CaseDetail {

	@Id
	@Column(name = "CASE_ID", scale = 10)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int caseId;
	
	@Column(name = "CARD_HOLDER_NAME", length = 50)
	@Nonnull
	private String cardHolderName;
	
	@Column(name = "CARD_NUMBER", length = 50)
	private String cardNumber;
	
	@Column(name = "ACCOUNT_NUMBER", length = 50)
	@Nonnull
	private String accountNumber;
	
	@Column(name = "ACCOUNT_TYPE", length = 50)
	@Nonnull
	private String accountType;
	
	@Column(name = "BALANCE_PRIOR_FRAUD")
	@Nonnull
	private double balPriorFraud;

	@Column(name = "TRANSACTION_TYPE", length = 50)
	@Nonnull
	private String transactionType;
	
	@Column(name = "TRANSACTION_AMOUNT", length = 50)
	@Nonnull
	private double transactionAmount;

	@Column(name = "FRAUD_DATE", length = 50)
	@Nonnull
	@Temporal(TemporalType.DATE)
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date fraudDate;
	
	@Column(name = "CASE_DATE", length = 50)
	@Nonnull
	@Temporal(TemporalType.DATE)
	private Date caseDate;
	
	@Column(name = "MERCHANT_NAME", length = 50)
	@Nonnull
	private String merchantName;
	
	@Column(name = "CARD_CANCELLED", length = 50)
	@Nonnull
	private String cardCancelled;
	
	@Column(name = "POSSESSION_WHEN_FRAUD_OCCURED", length = 50)
	@Nonnull
	private String possessionWhenFraudOccured;
	
	@Column(name = "PIN_SHARED", length = 50)
	@Nonnull
	private String pinShared;
	
	@Column(name = "CARD_LOST", length = 2000)
	@Nonnull
	private String cardLost;
	
	@Column(name = "TRANSACTION_OCCURED_DESC", length = 2000)
	@Nonnull
	private String descTransactionOccured;
	
	@Column(name = "ADDITIONAL_INFO", length = 2000)
	@Nonnull
	private String descAdditionalInfo;
	
	@Column(name = "REVIEWER_TYPE", length = 50)
	private String reviewerType;

	@Column(name="STATUS")
	private String status;
	
	@Column(name="CASE_POSSIBILITY",length=50)
    private String casePossibility;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "empId")
//	@JsonBackReference
	@JsonIdentityReference(alwaysAsId = true)
	private Employee employee;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "customerId" )
	@JsonBackReference
	private Customer customer;

	
	



	
}
