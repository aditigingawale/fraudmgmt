package com.fcm.krypton_bank.Repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fcm.krypton_bank.Entity.CaseDetail;
import com.fcm.krypton_bank.Entity.Customer;
import com.fcm.krypton_bank.Entity.Employee;

@Repository
public interface CaseDetailRepository extends JpaRepository<CaseDetail, Integer> {

	

	
	  List<CaseDetail> findByCustomerCustomerId(int customerId);
	
//	  //@Query("select case  from CaseDetail case where case.caseDate = :caseDate")
//	  Optional<CaseDetail> findByCaseDate(@Param("caseDate") Date caseDate);
	List<CaseDetail> findByCaseDate(@Param("caseDate") Date caseDate);
	  List<CaseDetail> findByEmployee(Employee employee);
	  
	  List<CaseDetail> findByStatus(String status);
	  

	  

	
}
