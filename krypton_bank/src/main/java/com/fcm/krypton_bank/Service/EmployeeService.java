package com.fcm.krypton_bank.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.fcm.krypton_bank.Entity.Employee;
import com.fcm.krypton_bank.Exception.EmailAlreadyExistException;
import com.fcm.krypton_bank.Repository.EmployeeRepository;

@Service
public class EmployeeService {
	private static final Logger logger = LogManager.getLogger(CustomerService.class);
	@Autowired
	private EmployeeRepository employeeRepository;
	PasswordEncoder passwordEncoder;

	public List<Employee> getAllEmployees() {
		List<Employee> employees = new ArrayList<>();
		Employee emp = new Employee();
		employees = employeeRepository.findAll();
		// employees.add(emp);
		return employees;
	}

	public Employee save(Employee employee)throws Exception {

		
		try {
			Employee emp = employeeRepository.findByEmail(employee.getEmail());
			if (emp != null) {
				throw new EmailAlreadyExistException("Email already Exist");
			}
			this.passwordEncoder = new BCryptPasswordEncoder();
			String encodedPassword = this.passwordEncoder.encode(employee.getPassword());
			employee.setPassword(encodedPassword);
			return employeeRepository.save(employee);
		}
		catch(EmailAlreadyExistException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
		

		
	}

	public void delete(long id) {
		employeeRepository.deleteById((int) id);
	}

	public Employee findById(long id) {
		Optional<Employee> empDb = employeeRepository.findById((int) id);
		return empDb.get();
	}

	public Employee getEmployeeById(int empid) {

		return employeeRepository.findById(empid).get();
	}

	public void saveOrUpdate(Employee employee) {

		employeeRepository.save(employee);

	}
}
