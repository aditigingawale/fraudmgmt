package com.fcm.krypton_bank.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.krypton_bank.Entity.EmployeeType;
import com.fcm.krypton_bank.Repository.EmployeeTypeRepository;

@Service
public class EmployeeTypeService {
	@Autowired
	private EmployeeTypeRepository employeeTypeRepository;
	 
	public void save(EmployeeType employeeType) {
		employeeTypeRepository.save(employeeType);
	}

	public EmployeeType getReviewers(int id) {
		Optional<EmployeeType> empType	=employeeTypeRepository.findById(  id);
		return empType.get();
	}

	
	

}
